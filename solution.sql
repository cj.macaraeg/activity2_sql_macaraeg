-- Start the Apache and MySQL in the XAMPP Control Pannel
-- Open the CMD, then type this code:

mysql -u root

-- Create a database named blog_db to create a table
CREATE DATABASE blog_db; 

-- Check if the database was successfully created
SHOW DATABASES;

-- Use the blog_db database to go inside on the database on it
USE blog_db;





-- Create table named users

CREATE TABLE users (
	id INT AUTO_INCREMENT NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
    datetime_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
);

-- Check if the table has been successfully created
SHOW TABLES;

-- CHeck if the description for the table is correct
DESCRIBE users;





-- Create table named posts
CREATE TABLE posts (
	id INT AUTO_INCREMENT NOT NULL,
	title VARCHAR(255) NOT NULL,
	content VARCHAR(255) NOT NULL,
	datetime_posted DATETIME DEFAULT CURRENT_TIMESTAMP,
    author_id INT,
	PRIMARY KEY(id),
		FOREIGN KEY(author_id) 
		REFERENCES users(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE
);
-- Check if the table has been successfully created
SHOW TABLES;

-- CHeck if the description for the table is correct
DESCRIBE posts;


-- Create table named posts_comments
CREATE TABLE posts_comments (
	id INT AUTO_INCREMENT NOT NULL,
	content VARCHAR(255) NOT NULL,
	datetime_commented DATETIME DEFAULT CURRENT_TIMESTAMP,
    post_id INT,
    user_id INT,
    PRIMARY KEY(id),
		FOREIGN KEY(post_id) 
		REFERENCES posts(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE,
		FOREIGN KEY(user_id) 
		REFERENCES posts(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE
);

-- Check if the table has been successfully created
SHOW TABLES;

-- CHeck if the description for the table is correct
DESCRIBE posts_comments;


-- Create table named posts_likes
CREATE TABLE posts_likes (
	id INT AUTO_INCREMENT NOT NULL,
	datetime_liked DATETIME DEFAULT CURRENT_TIMESTAMP,
    post_id INT,
    user_id INT,
    PRIMARY KEY(id),
		FOREIGN KEY(post_id) 
		REFERENCES posts(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE,
		FOREIGN KEY(user_id) 
		REFERENCES posts(id)
			ON UPDATE CASCADE
	    	ON DELETE CASCADE
);

-- Check if the table has been successfully created
SHOW TABLES;

-- CHeck if the description for the table is correct
DESCRIBE posts_likes;